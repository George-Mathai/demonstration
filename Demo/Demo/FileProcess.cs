﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;

namespace Demo
{
    /// <summary>
    /// The File Processing Class
    /// </summary>
    public class FileProcess
    {
        /// <summary>
        /// The fully qualified file path used for processing.
        /// </summary>
        public string fullFilePath { get; set; }

        /// <summary>
        /// The file data
        /// </summary>
        private DataTable data;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public FileProcess()
        {
            fullFilePath = String.Empty;
        }

        /// <summary>
        /// Checks if the file extension is the correct format and the file exists
        /// </summary>
        /// <returns>Checks if the file is valid and has the appropriate columns</returns>
        public Boolean validateAndLoadFile()
        {
            bool test1 = !(String.IsNullOrEmpty(fullFilePath));
            bool test2 = false;
            bool test3 = false;
            if (test1)
            {
                test2 = Path.GetExtension(fullFilePath).ToUpper() == ".CSV" && File.Exists(fullFilePath) && GetDataTableFromFile();
                test3 = data.Columns.Contains("FirstName") && data.Columns.Contains("LastName") && data.Columns.Contains("Address");
            }
            return test1 && test2 && test3;
        }

        /// <summary>
        /// Loads the file into the data table.
        /// </summary>
        /// <returns> Has the contents of the file loaded correctly. </returns>
        private bool GetDataTableFromFile()
        {
            try
            {
                string pathOnly = Path.GetDirectoryName(fullFilePath);
                string fileName = Path.GetFileName(fullFilePath);

                string sql = @"SELECT * FROM [" + fileName + "]";
                //Assumes that the first row is always the row titles.
                using (OleDbConnection connection = new OleDbConnection(
                    @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                    ";Extended Properties=\"Text;HDR=YES\""))
                using (OleDbCommand command = new OleDbCommand(sql, connection))
                using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
                {
                    data = new DataTable("Data");
                    data.Locale = CultureInfo.CurrentCulture;
                    adapter.Fill(data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Reading the file");
                Console.WriteLine("MESSAGE: " + ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Counts the number of times a name appears in the FirstName and LastName Column and writes to the file.
        /// </summary>
        public void CountNames()
        {
            try
            {
                var countNames = ((from x in data.AsEnumerable()
                                   group x by (string)x["FirstName"] into y
                                   select new
                                   {
                                       Key = y.Key,
                                       Count = y.Count()
                                   }
                                     ).ToList()
                                 ).Union(
                                     (from x in data.AsEnumerable()
                                      group x by (string)x["LastName"] into y
                                      select new
                                      {
                                          Key = y.Key,
                                          Count = y.Count()
                                      }
                                         ).ToList()
                                 );

                countNames = (from x in countNames.AsEnumerable()
                              group x by (string)x.Key into y
                              orderby y.Sum(n => n.Count) descending, y.Key ascending
                              select new
                              {
                                  Key = y.Key,
                                  Count = y.Sum(n => n.Count)
                              }
                              ).ToList();

                string pathOnly = Path.GetDirectoryName(fullFilePath);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(pathOnly + "file1.txt"))
                {
                    foreach (var count in countNames)
                    {
                        file.WriteLine(count.Key + ", " + count.Count);
                    }
                    Console.WriteLine("Created file: file1.txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Counting Names");
                Console.WriteLine("MESSAGE: " + ex.Message);
                throw(ex);
            }
        }

        /// <summary>
        /// Orders and prints the address to the file
        /// </summary>
        public void OrderAddresses()
        {
            try
            {
                var addresses = (from x in data.AsEnumerable()
                                 select new
                                 {
                                     StreetNumber = ((string)x["Address"]).Split(' ')[0],
                                     // Assumption: The first part will always be the street number.
                                     StreetAddress = ((string)x["Address"]).Replace(((string)x["Address"]).Split(' ')[0] + ' ', ""),
                                     // Assumption: The rest of the string will always be the street address.
                                     fullStreetAddress = (string)x["Address"]
                                 }
                                ).ToList();

                var sortedAddresses = (from x in addresses.AsEnumerable()
                                       orderby x.StreetAddress ascending, x.StreetNumber ascending
                                       select new { x.fullStreetAddress }
                             ).ToList();

                string pathOnly = Path.GetDirectoryName(fullFilePath);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(pathOnly + "file2.txt"))
                {
                    foreach (var address in sortedAddresses)
                    {
                        file.WriteLine(address.fullStreetAddress);
                    }
                    Console.WriteLine("Created file: file1.txt");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: Ordering Addresses");
                Console.WriteLine("MESSAGE: " + ex.Message);
                throw(ex);
            }
        }
    }
}
