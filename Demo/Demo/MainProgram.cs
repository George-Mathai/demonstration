﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Demo
{
    /// <summary>
    /// The Main Program
    /// </summary>
    class MainProgram
    {
        /// <summary>
        /// The entry program of the application.
        /// </summary>
        /// <param name="args">The list of arguments for the program.</param>
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                FileProcess fp = new FileProcess();
                fp.fullFilePath = args[0];
                Console.WriteLine("Processing file: \"" + fp.fullFilePath + "\"");

                if (fp.validateAndLoadFile())
                {
                    fp.CountNames();
                    fp.OrderAddresses();
                }
                else
                {
                    Console.WriteLine("ERROR: While validating and loading the file");
                    Console.WriteLine("See above for the error or the provided file does not exist or not in the correct format.");
                }
            }
            else
            {
                Console.WriteLine("Provide the fully qualified path of the CSV file.");
                Console.WriteLine("Demo.exe \"C:\\data.csv\"");
            }           
            Console.WriteLine("Press any key to stop.");
            Console.ReadKey();
        }
    }
}
