﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Demo;

namespace UnitTest
{
    [TestClass]
    public class UnitTest_FileProcess
    {
        private FileProcess fileProcess;

        /// <summary>
        /// Initialize the test
        /// </summary>
        [TestInitialize]
        public void SetUp()
        {
            fileProcess = new FileProcess();
        }

        /// <summary>
        /// Test failed validation from validation
        /// </summary>
        [TestMethod]
        public void TestMethodDefaultConstructorValidation()
        {
            bool returnVal = fileProcess.validateAndLoadFile();
            Assert.AreEqual(returnVal, false);
        }

        /// <summary>
        /// Test successful validation from validation
        /// </summary>
        [TestMethod]
        public void TestMethodValidateAndLoadFile()
        {
            fileProcess.fullFilePath="C:\\data.csv";
            bool returnVal = fileProcess.validateAndLoadFile();
            Assert.AreEqual(returnVal, true);
        }
    }
}
